package de.muehlena.joel.ui;

import de.muehlena.joel.Fahrstuhl;
import de.muehlena.joel.State;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FahrstuhlView extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../res/fxml/fahrstuhl.fxml"));
        Parent root = loader.load();
        FahrstuhlController controller = loader.getController();
        State.stateManager.add(controller);
        controller.setFahrstuhl(new Fahrstuhl());
        controller.setModel(new FahrstuhlModel());
        primaryStage.setTitle("Fahrstuhl");
        primaryStage.setScene(new Scene(root, 900, 1375));
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
        primaryStage.show();
    }

}
