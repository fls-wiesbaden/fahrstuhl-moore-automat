package de.muehlena.joel.ui;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FahrstuhlModel {

    private StringProperty floorIndicator = new SimpleStringProperty("0");


    public StringProperty floorIndicatorProperty() {
        if(floorIndicator == null) {
            this.floorIndicator = new SimpleStringProperty("");
        }

        return this.floorIndicator;
    }

    public void setFloorIndicator(String floorIndicator) {
        this.floorIndicator.set(floorIndicator);
    }
}
