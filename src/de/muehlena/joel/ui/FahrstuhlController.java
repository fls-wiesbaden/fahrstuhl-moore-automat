package de.muehlena.joel.ui;

import de.muehlena.joel.*;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.File;

public class FahrstuhlController implements IStateListener {
    private Fahrstuhl fahrstuhl;
    private FahrstuhlModel model;
    private File image = new File(Main.class.getResource("").getPath() + "res/images/aufzug.jpg");
    private File imageOpen = new File(Main.class.getResource("").getPath() + "res/images/aufzugOffen.jpg");
    private boolean isAnimating = false;

    @FXML
    private Label stage2IndicatorLabel;
    @FXML
    private Label stage1IndicatorLabel;
    @FXML
    private Label stage0IndicatorLabel;
    @FXML
    private Button floor2RequestButton;
    @FXML
    private Button floor1RequestButton;
    @FXML
    private Button floor0RequestButton;
    @FXML
    private ImageView aufzugImageView;

    public void setFahrstuhl(Fahrstuhl fahrstuhl) {
        this.fahrstuhl = fahrstuhl;
    }

    public void setModel(FahrstuhlModel model) {
        this.model = model;
        setbindings();
    }

    public void initialize() {
        this.floor0RequestButton.setStyle("-fx-background-color: #2ecc71");
        this.floor1RequestButton.setStyle("-fx-background-color: #2ecc71");
        this.floor2RequestButton.setStyle("-fx-background-color: #2ecc71");

        if(image.exists()) {
            Image pic = new Image(image.toURI().toString());
            this.aufzugImageView.setImage(pic);
        }else {
            System.out.println("cant load image");
        }

        this.aufzugImageView.setFocusTraversable(true);
    }

    private void setbindings() {
        stage0IndicatorLabel.textProperty().bind(model.floorIndicatorProperty());
        stage1IndicatorLabel.textProperty().bind(model.floorIndicatorProperty());
        stage2IndicatorLabel.textProperty().bind(model.floorIndicatorProperty());
    }

    private void animateElevator() {

        int direction = 0;

        if(State.UP == 1) {
        direction = -200;
        }else if(State.DOWN == 1) {
            direction = 200;
        }

        TranslateTransition tt = new TranslateTransition();
        tt.setNode(aufzugImageView);
        tt.setFromY(aufzugImageView.getTranslateY());
        tt.setToY(aufzugImageView.getTranslateY() + direction);
        tt.setDuration(new Duration(5000));
        tt.play();
    }

    private void openElevatorDoor() {
        aufzugImageView.setImage(new Image(imageOpen.toURI().toString()));
    }

    private void closeElevatorDoor() {
        aufzugImageView.setImage(new Image(image.toURI().toString()));
    }

    @FXML
    public void require2(ActionEvent e) {
        System.out.println("Required on stage 2");
        State.setR2(1);
        this.fahrstuhl.startElevator();
    }

    @FXML
    public void require1(ActionEvent e) {
        System.out.println("Required on stage 1");
        State.setR1(1);
        this.fahrstuhl.startElevator();
    }

    @FXML
    public void requireEG(ActionEvent e) {
        System.out.println("Required on stage EG (0)");
        State.setR0(1);
        this.fahrstuhl.startElevator();
    }


    @Override
    public void update(Object o) { }

    @Override
    public void onChangeFloorIndicator(Object o) {
        model.setFloorIndicator((String) o);
    }

    @Override
    public void onAnimateElevatorEvent(ElevatorEvent elevatorEvent) {

        if(elevatorEvent.getDirection().equals("UP") || elevatorEvent.getDirection().equals("DOWN")) {
            animateElevator();
        }
    }

    @Override
    public void onOpenDoor() {
        this.openElevatorDoor();
    }

    @Override
    public void onCloseDoor() {
        this.closeElevatorDoor();
    }

}
