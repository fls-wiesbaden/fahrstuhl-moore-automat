package de.muehlena.joel;

import javafx.application.Platform;

public class Fahrstuhl {

    private Thread currentDriveT = null;

    public void startElevator () {
       if(currentDriveT == null || !currentDriveT.isAlive()) {

          if((State.E0 == 1 && (State.R1 == 1 || State.R2 == 1)) || (State.E1 == 1 && State.R2 == 1)) {
              State.setUP(1);
          }else if((State.E2 == 1 && (State.R1 == 1 || State.R0 == 1)) || (State.E1 == 1 && State.R0 == 1)) {
              State.setDOWN(1);
          }

           Thread t = new Thread(new DriveThread(State.UP == 1 ? 1 : 0));
           this.currentDriveT = t;
           t.start();
       }else {
           if(State.queue == -1 && false) {
               if( State.DOWN == 1 && (State.E1 == 1 && State.R2 == 1)) {
                   State.queue = 1;
               }else if(State.UP == 1 && ((State.E1 == 1 && State.R0 == 1) || (State.E2 == 1 && (State.R1 == 1 || State.R0 == 1)))) {
                   State.queue = 2;
               }
           }

           System.out.println("Queue: " + State.queue);
        }
    }

}
