package de.muehlena.joel;

import de.muehlena.joel.ui.FahrstuhlView;
import javafx.application.Application;

public class Main {


    public static void main(String[] args) {
        Application.launch(FahrstuhlView.class, args);
    }
}
