package de.muehlena.joel;

import de.muehlena.joel.ui.FahrstuhlController;

import java.util.ArrayList;
import java.util.List;

public class State {

    public static List<FahrstuhlController> stateManager = new ArrayList<>();

    public static int E0 = 1;
    public static int E1 = 0;
    public static int E2 = 0;
    public static int R0 = 0;
    public static int R1 = 0;
    public static int R2 = 0;
    public static int UP = 0;
    public static int DOWN = 0;
    public static int queue = -1;
    public static String MSG = "";

    public static void setDOWN(int DOWN) {
        State.DOWN = DOWN;

        for (FahrstuhlController controller : stateManager) {
            controller.onAnimateElevatorEvent(new ElevatorEvent("DOWN"));
        }
    }

    public static void setMSG(String MSG) {
        State.MSG = MSG;

        for (FahrstuhlController controller : stateManager) {
            controller.update(State.MSG);
        }
    }

    public static void triggerAnimation () {
        for (FahrstuhlController controller : stateManager) {
            controller.onAnimateElevatorEvent(State.DOWN == 1 ? new ElevatorEvent("DOWN") : new ElevatorEvent("UP"));
        }
    }

    public static void openDoor() {
        for (FahrstuhlController controller : stateManager) {
            controller.onOpenDoor();
        }
    }

    public static void closeDoor() {
        for (FahrstuhlController controller : stateManager) {
            controller.onCloseDoor();
        }
    }

    public static void setUP(int UP) {
        State.UP = UP;

        for (FahrstuhlController controller : stateManager) {
            controller.onAnimateElevatorEvent(new ElevatorEvent("UP"));
        }
    }

    public static void setR0(int r0) {
        R0 = r0;

        for (FahrstuhlController controller : stateManager) {
            controller.update("R0");
        }
    }

    public static void setR1(int r1) {
        R1 = r1;

        for (FahrstuhlController controller : stateManager) {
            controller.update("R1");
        }
    }

    public static void setR2(int r2) {
        R2 = r2;

        for (FahrstuhlController controller : stateManager) {
            controller.update("R2");
        }
    }

    public static void setE0(int e0) {
        E0 = e0;

        for (FahrstuhlController controller : stateManager) {
            controller.onChangeFloorIndicator("0");
        }
    }

    public static void setE1(int e1) {
        E1 = e1;

        for (FahrstuhlController controller : stateManager) {
            controller.onChangeFloorIndicator("1");
        }
    }

    public static void setE2(int e2) {
        E2 = e2;

        for (FahrstuhlController controller : stateManager) {
            controller.onChangeFloorIndicator("2");
        }
    }
}
