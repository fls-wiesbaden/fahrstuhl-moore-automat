package de.muehlena.joel;

public interface IStateListener {
     void update(Object o);
     void onChangeFloorIndicator(Object o);
     void onAnimateElevatorEvent(ElevatorEvent elevatorEvent);
     void onOpenDoor();
     void onCloseDoor();
}

