package de.muehlena.joel;

import javafx.application.Platform;

public class DriveThread implements Runnable{

    //0 down;1 up
    public int direction = 0;

    public DriveThread(int direction) {
        this.direction = direction;
    }

    @Override
    public void run() {

        System.out.println("Direction: " + direction);

        switch (this.direction) {
            case 1:
                moveUp();
                System.out.println("stopped moving");
                break;
            case 0:
                moveDown();
                System.out.println("stop moving");
                break;
        }

        if(State.queue == 1) {
            moveUp();
        }else if(State.queue == 2) {
            moveDown();
        }else {
            State.queue = -1;
            Thread.currentThread().interrupt();
        }
    }

    public void moveDown() {
        int duration = 5;

        long startMoveTime = System.currentTimeMillis();
        long endTime = startMoveTime + duration * 1000;

        Platform.runLater(State::closeDoor);

        while(startMoveTime < endTime) {
            startMoveTime = System.currentTimeMillis();
        }

        //if elevator is in second floor
        if(State.E2 == 1) {

            //Set state to arrived on floor 1
            Platform.runLater(() -> {
                State.setE2(0);
                State.setE1(1);
            });

            //if floor 1 is requested open door
            if(State.R1 == 1) {
                Platform.runLater(() -> {
                    State.setR1(0);
                    State.openDoor();
                });
                try {
                    Thread.sleep(3000);
                    Platform.runLater(State::closeDoor);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // if floor 0 is requested move down through recursion
            if(State.R0 == 1) {
                Platform.runLater(State::triggerAnimation);
                moveDown();
            }else {
                Platform.runLater(() -> State.setDOWN(0));
            }
        //if elevator is in first floor
        }else if(State.E1 == 1){

            //set rquested to 0
            if(State.R0 == 1) {
                Platform.runLater(() -> {
                    State.setR0(0);
                    State.openDoor();
                });

            }

            //set state to arrived at eg
            Platform.runLater(() -> {
                State.setE1(0);
                State.setDOWN(0);
                State.setE0(1);
            });

        }
    }

    public void moveUp() {
        int duration = 5;

        Platform.runLater(State::closeDoor);

        long startMoveTime = System.currentTimeMillis();
        long endTime = startMoveTime + duration * 1000;

        while(startMoveTime < endTime) {
            startMoveTime = System.currentTimeMillis();
        }

        //if elevator is in eg
        if(State.E0 == 1) {

            //Set State to arrived at fllor 1
            Platform.runLater(() -> {
                State.setE0(0);
                State.setE1(1);
            });

            //is floor 1 requested
            if(State.R1 == 1) {
                Platform.runLater(() -> {
                    State.setR1(0);
                    State.openDoor();
                });
                System.out.println("open on floor 1");
                try {
                    Thread.sleep(3000);
                    Platform.runLater(State::closeDoor);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if(State.R2 == 1) {
                System.out.println("move on to scond");
                Platform.runLater(State::triggerAnimation);
                moveUp();
            }else {
                Platform.runLater(() -> State.setUP(0));
            }
        //if elevator is in first floor
        }else if(State.E1 == 1){
            Platform.runLater(() -> {
                State.setE1(0);
                State.setUP(0);
                State.setE2(1);
            });

            if(State.R2 == 1) {
                Platform.runLater(() -> {
                    State.setR2(0);
                    State.openDoor();
                });
            }
        }
    }

}
