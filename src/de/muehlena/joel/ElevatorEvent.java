package de.muehlena.joel;

public class ElevatorEvent {

    public final String direction;

    ElevatorEvent(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
